FROM node:16
WORKDIR /app
COPY . . 
RUN npm install && npm run build
CMD ["node", "dist/main"]
EXPOSE 3000